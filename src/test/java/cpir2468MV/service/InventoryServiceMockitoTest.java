package cpir2468MV.service;

import cpir2468MV.model.InhousePart;
import cpir2468MV.model.Part;
import cpir2468MV.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InventoryServiceMockitoTest {
    private InventoryRepository repo;
    private InventoryService service;

    @BeforeEach
    void setUp() {
        repo = mock(InventoryRepository.class);
        service = new InventoryService(repo);
    }

    @AfterEach
    void tearDown() {
        repo = null;
        service = null;
    }

    @Test
    void lookupPart() {
        InhousePart part = new InhousePart(1, "1", 5, 3, 1, 5, 1);
        ObservableList<Part> list = FXCollections.observableArrayList();
        list.add(part);
        Mockito.when(repo.lookupPart("1")).thenReturn(part);
        Mockito.when(repo.getAutoPartId()).thenReturn(1);

        service.addInhousePart("1", 1, 1, 5, 3, 1);

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(0)).lookupPart("1");

        assertEquals(service.lookupPart("1"), part);

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(1)).lookupPart("1");
    }

    @Test
    void deletePart() {
        InhousePart part = new InhousePart(1, "1", 5, 3, 1, 5, 1);
        ObservableList<Part> list = FXCollections.observableArrayList();

        Mockito.when(repo.getAutoPartId()).thenReturn(1);
        Mockito.when(repo.getAllParts()).thenReturn(list);
        Mockito.doNothing().when(repo).deletePart(part);

        service.addInhousePart("1", 1, 1, 5, 3, 1);

        Mockito.verify(repo, times(0)).deletePart(part);
        service.deletePart(part);

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(0)).getAllParts();
        Mockito.verify(repo, times(1)).deletePart(part);

        assertEquals(service.getAllParts().size(), 0);

        Mockito.verify(repo, times(1)).getAutoPartId();
        Mockito.verify(repo, times(1)).getAllParts();
    }
}