package cpir2468MV.service;

import cpir2468MV.model.InhousePart;
import cpir2468MV.repository.InventoryRepository;
import org.junit.jupiter.api.*;

import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTest {
    private InventoryService service;
    private InventoryRepository repository;

    @BeforeEach
    public void setUp(){
        repository = new InventoryRepository("data/itemstest.txt");
        service = new InventoryService(repository);
    }

    @AfterEach
    public void tearDown() throws IOException {
        repository = null;
        new FileWriter("D:\\Facultate\\Anul3\\Semestrul2\\VVSS\\cpir2468\\target\\classes\\data\\itemstest.txt", false).close();
    }

    @Nested
    @DisplayName("ECP TEST CLASS")
    class ECP_Test{
        @Test
        @DisplayName("Test 1 ECP Valid Assert Name")
        void addPartTest1ECP(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "testpart1", 5, 5, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertTrue(repository.lookupPart(inhousePart.getName()).getName().length() >= 1 && repository.lookupPart(inhousePart.getName()).getName().length() <= 255);
        }

        @Test
        @DisplayName("Test 2 ECP Valid Assert Stock")
        void addPartTest2ECP(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "testpart2", 5, 5, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertTrue(repository.lookupPart(inhousePart.getName()).getInStock() >= repository.lookupPart(inhousePart.getName()).getMin() && repository.lookupPart(inhousePart.getName()).getInStock() <= repository.lookupPart(inhousePart.getName()).getMax());
        }

        @Test
        @DisplayName("Test 3 ECP Non-Valid Assert Name")
        void addPartTest3ECP(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "", 5, 5, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertFalse(repository.lookupPart(inhousePart.getName()).getName().length() >= 1 && repository.lookupPart(inhousePart.getName()).getName().length() <= 255);
        }

        @Test
        @DisplayName("Test 4 ECP Non-Valid Assert Stock")
        void addPartTest4ECP(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "testpart4", 5, 0, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertFalse((repository.lookupPart(inhousePart.getName()).getInStock() >= inhousePart.getMin() && repository.lookupPart(inhousePart.getName()).getInStock() <= inhousePart.getMax()));
        }
    }

    @Nested
    @DisplayName("BVA TEST CLASS")
    class BVA_Test{
        @Test
        @DisplayName("Test 1 BVA Valid Assert Name")
        void addPartTest1BVA(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "1", 5, 5, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertEquals(1, repository.lookupPart(inhousePart.getName()).getName().length());
        }

        @Test
        @DisplayName("Test 2 BVA Valid Assert Stock")
        void addPartTest2BVA(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "testpart2", 5, 10, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertEquals(10, repository.lookupPart(inhousePart.getName()).getInStock());
        }

        @Test
        @DisplayName("Test 3 BVA Non-Valid Assert Name")
        void addPartTest3BVA(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "", 5, 5, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertEquals(0, repository.lookupPart(inhousePart.getName()).getName().length());
        }

        @Test
        @DisplayName("Test 4 BVA Non-Valid Assert Stock")
        void addPartTest4BVA(){
            InhousePart inhousePart = new InhousePart(repository.getInventory().getAutoPartId(), "testpart4", 5, 0, 1, 10, 1);
            service.addInhousePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax(), inhousePart.getMachineId());
            assertEquals(0, repository.lookupPart(inhousePart.getName()).getInStock());
        }
    }
}