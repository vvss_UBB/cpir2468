package cpir2468MV.service;

import cpir2468MV.model.Product;
import cpir2468MV.repository.InventoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceLookupTest {
    private InventoryService service;
    private InventoryRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InventoryRepository("data/itemsfull.txt");
        service = new InventoryService(repository);
    }

    @AfterEach
    void tearDown() {
        repository = null;
        service = null;
    }

    @Test
    void lookupProductTest1() {
        Product p = service.lookupProduct("1");
        assertEquals(Integer.toString(p.getProductId()), "1");
    }

    @Test
    void lookupProductTest2() {
        Product p = service.lookupProduct("Clock");
        assertEquals(p.getName(), "Clock");
    }

    @Test
    void lookupProductTest3() {
        Product p = service.lookupProduct("-1");
        assertNull(p);
    }

    @Test
    void lookupProductTest4() {
        repository = new InventoryRepository("data/itemsnull.txt");
        service = new InventoryService(repository);
        Product p = service.lookupProduct("1");
        assertEquals(p.getProductId(), 0);
    }

    @Test
    void lookupProductTest5() {
        Product p = service.lookupProduct("products2");
        assertEquals(p.getName(), "products2");
    }

    @Test
    void lookupProductTest6() {
        Product p = service.lookupProduct("products5");
        assertEquals(p.getName(), "products5");
    }

    @Test
    void lookupProductTest7() {
        Product p = service.lookupProduct("products6");
        assertEquals(p.getName(), "products6");
    }

    @Test
    void lookupProductTest8() {
        Product p = service.lookupProduct("products7");
        assertEquals(p.getName(), "products7");
    }

    @Test
    void lookupProductTest9() {
        Product p = service.lookupProduct("products4");
        assertEquals(p.getName(), "products4");
    }

    @Test
    void lookupProductTest10() {
        Product p = service.lookupProduct("products13");
        assertEquals(p.getName(), "products13");
    }

    @Test
    void lookupProductTest11() {
        Product p = service.lookupProduct("products14");
        assertEquals(p.getName(), "products14");
    }
}