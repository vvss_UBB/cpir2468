package cpir2468MV.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OutsourcedPartTest {

    private OutsourcedPart part;

    @BeforeEach
    void setUp() {
        part = new OutsourcedPart(1, "1", 3, 3, 1, 5, "company");
    }

    @AfterEach
    void tearDown() {
        part = null;
    }

    @Test
    void getCompanyName() {
        assertEquals(part.getCompanyName(), "company");
    }

    @Test
    void setCompanyName() {
        part.setCompanyName("company2");
        assertEquals(part.getCompanyName(), "company2");
    }
}