package cpir2468MV.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = new Inventory();
    }

    @AfterEach
    void tearDown() {
        inventory = null;
    }

    @Test
    void addPart() {
        InhousePart inhousePart = new InhousePart(1, "testpart1", 5, 5, 1, 10, 1);
        inventory.addPart(inhousePart);

        assertEquals(inventory.getAllParts().size(), 1);
    }

    @Test
    void deletePart() {
        InhousePart inhousePart = new InhousePart(1, "testpart1", 5, 5, 1, 10, 1);
        inventory.addPart(inhousePart);
        assertEquals(inventory.getAllParts().size(), 1);
        inventory.deletePart(inhousePart);
        assertEquals(inventory.getAllParts().size(), 0);
    }
}