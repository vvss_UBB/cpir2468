package cpir2468MV.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InhousePartTest {
    private InhousePart part;

    @BeforeEach
    void setUp(){
        part = new InhousePart(1, "1", 5, 3, 1, 5, 1);
    }

    @AfterEach
    void tearDown(){
        part = null;
    }

    @Test
    void getMachineId() {
        assertEquals(part.getMachineId(), 1);
    }

    @Test
    void setMachineId() {
        part.setMachineId(2);
        assertEquals(part.getMachineId(), 2);
    }
}