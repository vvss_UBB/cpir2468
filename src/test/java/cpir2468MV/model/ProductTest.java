package cpir2468MV.model;

import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    private Product product;
    ObservableList<Part> parts;

    @BeforeEach
    void setUp() {
        product = new Product(1, "part", 5, 5, 1, 10, parts);
    }

    @AfterEach
    void tearDown() {
        product = null;
    }

    @Test
    void getPrice() {
        assertEquals(product.getPrice(), 5);
    }

    @Test
    void setPrice() {
        product.setPrice(10);
        assertEquals(product.getPrice(), 10);
    }
}